# dealer_import

## Description
* The app imports docs/dealer_import.csv to storage/dealer_import.db
* Import results are printed to stdout
* The original data are kept in docs/ and a copy of the db is made to storage/ (not committed)

## How to Run
* python3.7 + venv is the easiest
    * Docker works if you use VSCode w/ remote-containers extension
* `pip install -r requirements.txt`
* `scripts/run.sh` - if storage/ doesn't exist, makes a copy of db in storage/ and imports csv into it
* `scripts/reset.sh` - re-sets up /storage
* `scripts/test.sh` - runs pytest

## Notes & Assumptions
* Extra csv fields: - See Compromises
    * certified
    * dateinstock
* Assume 'CVT' === 'Variable' === 'Automatic'
* KM8SB12B02U162029 has '6-Speed Automatic Manual Shift', assume it means 'auto w/ manual paddles' therefore Automatic
* Should we discern between different types of pickups? assume all maps to 'Pickup'
* How does 1GCEP22T1G3329139 have a cvt but is also a 6spd? gonna put in None for this
* Hardcoding 'modified_by' to 'IMPORT'

## Design Choices & Compromises
* Prioritized extensibility for mapping csv columns to vehicle attributes with reusable processors e.g. to_upper
* Using PeeWee ORM
    * Eliminated need for persistence & db; persistence info embedded in model
    * Unfortunately created strong coupling between the domain model and orm provider
    * Aware of differing varchar lengths; did not specify in model (in the interest of time)
* Using vehicle's VIN instead of id as the real unique identifier
* Granular error handling: an error for one vehicle in the batch should not invalidate the whole batch
    * Service returns import results e.g. created/updated/failed with error message for each vin
* Slight discrepancy between created & modified timestamps as datetime.now called twice
    * maybe timestamp should be the same for the entire import session
* Needs better module layout; for convenience everything in one module
    * removed wildcard import statements; caused assemblers' import statement to be long
* Did TDD for the trick Mapper classes; skiped testing for others in the interest of time
* Hardcoded db path for convenience
    * Same for csv location
* Did not modify existing db schema therefore ignored 'Certified' & 'DateInStock'
    * Next step: http://docs.peewee-orm.com/en/latest/peewee/playhouse.html?#schema-migrations
* For all the fields, if the input is falsy, it's imported as None
    * Attributes like 'vin' should have a check for invalid values
* db connections lifecycle is per vehicle import, which has overhead
* ImportService class definitely could use unit tests w/ Vehicle mocks

## Ideas
* Add some functional tests instead of EyeballQA: https://pypi.org/project/pytest-bdd/
* Chalice -> wraps api around service layer instead of app.py's main being the entrypoint
* GitLab CI - hook into tests
* Dependency injection once things get complex enough
* db normalization e.g. separate dealer table w/ d_id as fk; helps with having single sources of truth
    * if the dealer changes his shop name it's easier to update
* Add index to vin for better query performance
* How to accomodate Electric cars - different but overlapping set of specs - cylinders? transmission? fuel type? battery capacity? range?
    * Use inheritance (simple db solution: https://en.wikipedia.org/wiki/Single_Table_Inheritance)
* Odometer has ambiguous unit without reading docs (ditto others)
* Room for improvement with attribute names e.g. 'configuration' is vague even within the vehicle context, 'engine_configuration' is more explicit
* Current Dockerfile is for dev only; fairly simple to give an ENTRYPOINT & CMD to run the import task/service

## Setup
* Win10 Pro 1809
    * Docker Desktop CE 2.0.0.3
* VSCode (PREVIEW) plugins:
    * Docker
    * ms-vscode-remote.remote-containers
    * ms-vscode-remote.vscode-remote-extensionpack