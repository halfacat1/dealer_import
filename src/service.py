from .model import Vehicle, db
from .assemblers import UpdateVehicleFromCsvRow
from datetime import datetime


class ImportService:
    def __init__(self):
        self.update_vehicle_from_row = UpdateVehicleFromCsvRow()

    def import_rows(self, rows):
        results = []
        for row in rows:
            results.append(self.import_row(row))
        return results

    def import_row(self, row):
        vin = row['VIN']
        result = {'vin': vin, 'status': 'updated'}
        try:
            db.connect()
            vehicle = Vehicle.get_or_none(Vehicle.vin == vin)
            if not vehicle:
                vehicle = Vehicle.create()
                vehicle.created_time = datetime.now()
                result['status'] = 'created'
            self.update_vehicle_from_row.update(row, vehicle)
            vehicle.last_modified_time = datetime.now()
            vehicle.last_modified_by = 'IMPORT'
            vehicle.save()
        except BaseException as e:
            result['status'] = 'error'
            result['error'] = e
        finally:
            db.close()
            return result
