import re
import functools


def bypass_falsy_input(func):
    def wrapper(*args, **kwargs):
        if not args[1]:
            return ''
        return func(*args, **kwargs)

    return wrapper


class VerbatimMapper:
    def map(self, input):
        return input


class ToUpperMapper:
    @bypass_falsy_input
    def map(self, input):
        return str.upper(input)


class ToLowerMapper:
    @bypass_falsy_input
    def map(self, input):
        return str.lower(input)


class ToTitleMapper:
    @bypass_falsy_input
    def map(self, input):
        return input.title()


class DisplacementMapper:
    @bypass_falsy_input
    def map(self, input):
        match = re.search(r"^([0-9]+[.]*[0-9]*) ?L$", input).groups()[0]
        return float(match)


class DrivetrainMapper:
    @bypass_falsy_input
    def map(self, input):
        return f'{input[0].upper()}WD'


class BodyStyleMapper:
    @bypass_falsy_input
    def map(self, input):
        regex_pairs = [(r"^.*Sport Utility.*$", 'Sport Utility Vehicle'),
                       (r"^.*Cargo Van.*$", 'Cargo Van'),
                       (r"^.*Car.*$", 'Car'),
                       (r"^.*Station Wagon.*$", 'Station Wagon'),
                       (r"^.*Convertible.*$", 'Convertible'),
                       (r"^.*Pickup.*$", 'Pickup')]
        try:
            match = next(p for p in regex_pairs if re.search(p[0], input))
            return match[1]
        except StopIteration:
            raise Exception(f'Invalid body style: {input}')


class TransmissionTypeMapper:
    @bypass_falsy_input
    def map(self, input):
        if 'auto' in input.lower() or 'cvt' in input.lower():
            return 'Automatic'
        else:
            return 'Manual'


class TransmissionSpeedsMapper:
    @bypass_falsy_input
    def map(self, input):
        match = re.search(r"^([0-9]+)-.*$", input)
        if not match:
            return None
        return int(match.groups()[0])


class TransmissionDescriptionMapper:
    @bypass_falsy_input
    def map(self, input):
        speeds = TransmissionSpeedsMapper().map(input)
        if not speeds:
            return 'Variable'
        has_auto = 'auto' in input.lower()
        has_manual = 'manual' in input.lower()
        
        # if has_auto and has_manual:
        #     return f'{speeds}-Speed Automatic Manual Shift'
        # if has_auto:
        #     return f'{speeds}-Speed Automatic'
        #     if has_auto:
        
        # I think the above is actually more descriptive but this is cooler
        return f"{speeds}-Speed{' Automatic' if has_auto else ''}{' Manual' if has_manual else ''}{' Shift' if has_auto and has_manual else ''}"