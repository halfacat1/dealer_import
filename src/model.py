import peewee as pw

# Do not hardcode!
db = pw.SqliteDatabase('storage/dealer_import.db')

class Vehicle(pw.Model):
    v_id = pw.PrimaryKeyField()
    d_id = pw.IntegerField()
    d_name = pw.CharField()
    stock_type = pw.CharField()
    stock_id = pw.CharField()
    vin = pw.CharField()
    year = pw.IntegerField()
    make = pw.CharField()
    model = pw.CharField()
    body_style = pw.CharField()
    trim = pw.CharField()
    doors = pw.IntegerField()
    exterior_colour = pw.CharField()
    interior_colour = pw.CharField()
    exterior_colour_generic = pw.CharField()
    interior_colour_generic = pw.CharField()
    configuration = pw.CharField()
    cylinders = pw.IntegerField()
    displacement = pw.DoubleField()
    fuel_type = pw.CharField()
    transmission_type = pw.CharField()
    transmission_speeds = pw.IntegerField()
    transmission_description = pw.CharField()
    drivetrain = pw.CharField()
    odometer = pw.IntegerField()
    price = pw.DoubleField()
    msrp = pw.DoubleField()
    description = pw.CharField()
    passengers = pw.IntegerField()
    created_time = pw.DateTimeField()
    last_modified_by = pw.DateTimeField()
    last_modified_time = pw.DateTimeField()

    class Meta:
        database = db
        table_name = 'inventory'