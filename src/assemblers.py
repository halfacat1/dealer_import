from .model import Vehicle
from .string_mappers import VerbatimMapper, ToUpperMapper, ToLowerMapper, ToTitleMapper, DrivetrainMapper, BodyStyleMapper, DisplacementMapper, TransmissionTypeMapper, TransmissionSpeedsMapper, TransmissionDescriptionMapper


class UpdateVehicleFromCsvRow:
    def __init__(self):
        verbatim = VerbatimMapper()
        to_upper = ToUpperMapper()
        to_lower = ToLowerMapper()
        to_title = ToTitleMapper()
        drive_train = DrivetrainMapper()
        body_style = BodyStyleMapper()
        displacement = DisplacementMapper()
        transmission_type = TransmissionTypeMapper()
        transmission_speeds = TransmissionSpeedsMapper()
        transmission_description = TransmissionDescriptionMapper()
        
        self.mapping = [
            ('d_id', lambda row: verbatim.map(row.get('DealerID'))),
            ('d_name', lambda row: verbatim.map(row.get('DealerName'))),
            ('stock_type', lambda row: to_upper.map(row.get('Type'))),
            ('stock_id', lambda row: verbatim.map(row.get('Stock'))),
            ('vin', lambda row: verbatim.map(row.get('VIN'))),
            ('year', lambda row: verbatim.map(row.get('Year'))),
            ('make', lambda row: verbatim.map(row.get('Make'))),
            ('model', lambda row: verbatim.map(row.get('Model'))),
            ('body_style', lambda row: body_style.map(row.get('Body'))),
            ('trim', lambda row: verbatim.map(row.get('Trim'))),
            ('doors', lambda row: verbatim.map(row.get('Doors'))),
            ('exterior_colour', lambda row: to_title.map(row.get('ExtColor'))),
            ('interior_colour', lambda row: to_title.map(row.get('IntColor'))),
            ('cylinders', lambda row: verbatim.map(row.get('EngCylinders'))),
            ('displacement', lambda row: displacement.map(row.get('EngDisplacement'))),
            ('transmission_type', lambda row: transmission_type.map(row.get('Transmission'))),
            ('transmission_speeds', lambda row: transmission_speeds.map(row.get('Transmission'))),
            ('transmission_description', lambda row: transmission_description.map(row.get('Transmission'))),
            ('odometer', lambda row: verbatim.map(row.get('Odometer'))),
            ('price', lambda row: verbatim.map(row.get('Price'))),
            ('msrp', lambda row: verbatim.map(row.get('MSRP'))),
            ('description', lambda row: verbatim.map(row.get('Description'))),
            ('configuration', lambda row: verbatim.map(row.get('EngType'))),
            ('fuel_type', lambda row: verbatim.map(row.get('EngFuel'))),
            ('drivetrain', lambda row: drive_train.map(row.get('Drivetrain'))),
            ('exterior_colour_generic', lambda row: to_lower.map(row.get('ExtColorGeneric'))),
            ('interior_colour_generic', lambda row: to_lower.map(row.get('IntColorGeneric'))),
            ('passengers', lambda row: verbatim.map(row.get('PassengerCount'))),
        ]

    def update(self, row, vehicle):
        for map in self.mapping:
            setattr(vehicle, map[0], map[1](row))