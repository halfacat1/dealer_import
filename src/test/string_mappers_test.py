import pytest
from ..string_mappers import *


def test_verbatim():
    assert VerbatimMapper().map('hEllO0') == 'hEllO0'


def test_to_upper():
    assert ToUpperMapper().map('hEllO0') == 'HELLO0'


def test_to_lower():
    assert ToLowerMapper().map('hEllO0') == 'hello0'


def test_to_title():
    assert ToTitleMapper().map('black car') == 'Black Car'
    assert ToTitleMapper().map('BLACK car') == 'Black Car'


def test_displacement():
    assert DisplacementMapper().map('3.0L') == 3
    assert DisplacementMapper().map('3.0 L') == 3
    assert DisplacementMapper().map('3.5L') == 3.5
    assert DisplacementMapper().map('3.5 L') == 3.5
    assert DisplacementMapper().map('15.23 L') == 15.23


def test_drivetrain():
    assert DrivetrainMapper().map('4WD') == '4WD'
    assert DrivetrainMapper().map('4 wheel drive') == '4WD'
    assert DrivetrainMapper().map('AWD') == 'AWD'
    assert DrivetrainMapper().map('RWD') == 'RWD'
    assert DrivetrainMapper().map('FWD') == 'FWD'


def test_body_style():
    assert BodyStyleMapper().map('Sport Utility') == 'Sport Utility Vehicle'
    assert BodyStyleMapper().map('Crew Cab Pickup') == 'Pickup'
    assert BodyStyleMapper().map('4dr Car') == 'Car'
    assert BodyStyleMapper().map('Cargo Van') == 'Cargo Van'
    assert BodyStyleMapper().map('Extended Cab Pickup') == 'Pickup'
    assert BodyStyleMapper().map('Station Wagon') == 'Station Wagon'
    assert BodyStyleMapper().map('Convertible') == 'Convertible'
    assert BodyStyleMapper().map('2dr Car') == 'Car'
    assert BodyStyleMapper().map('Car') == 'Car'


def test_body_style_raise_on_invalid_input():
    with pytest.raises(Exception) as e:
        BodyStyleMapper().map('submarine')
        assert e.value == 'Invalid body style: submarine'


def test_transmission_type():
    assert TransmissionTypeMapper().map('10-Speed Auto-Shift Manual w/OD') == 'Automatic'
    assert TransmissionTypeMapper().map('CVT') == 'Automatic'
    assert TransmissionTypeMapper().map('6-Speed Manual w/OD') == 'Manual'
    assert TransmissionTypeMapper().map('8-Speed Automatic w/OD') == 'Automatic'
    assert TransmissionTypeMapper().map('6-Speed Automatic w/Manual Shift') == 'Automatic'


def test_transmission_speeds():
    assert TransmissionSpeedsMapper().map('CVT') is None
    assert TransmissionSpeedsMapper().map('5-Speed Auto-Shift Manual w/OD') == 5
    assert TransmissionSpeedsMapper().map('10-Speed Auto-Shift Manual w/OD') == 10
    assert TransmissionSpeedsMapper().map('5-Spd Automatic') == 5


def test_transmission_description():
    assert TransmissionDescriptionMapper().map('CVT') == 'Variable'
    assert TransmissionDescriptionMapper().map('6-Speed Manual w/OD') == '6-Speed Manual'
    assert TransmissionDescriptionMapper().map('6-Spd Manual') == '6-Speed Manual'
    assert TransmissionDescriptionMapper().map('6-Spd Automatic') == '6-Speed Automatic'
    assert TransmissionDescriptionMapper().map('6-Speed Automatic w/OD') == '6-Speed Automatic'
    assert TransmissionDescriptionMapper().map('6-Speed Automatic w/Manual Shift') == '6-Speed Automatic Manual Shift'
    assert TransmissionDescriptionMapper().map('10-Speed Auto-Shift Manual w/OD') == '10-Speed Automatic Manual Shift'