# https://code.visualstudio.com/docs/remote/containers
FROM python:3.7

# Skip interactive steps for apt
ENV DEBIAN_FRONTEND=noninteractive

ENV SHELL /bin/bash
RUN apt-get update && \
    apt-get install -y --no-install-recommends apt-utils
RUN pip install pylint && \
    pip install yapf

# Install git, process tools, lsb-release (common in install instructions for CLIs)
RUN apt-get install -y git procps lsb-release

# Install any missing dependencies for enhanced language service
RUN apt-get install -y libicu[0-9][0-9]

# Set up workspace
RUN mkdir /workspaces
WORKDIR /workspaces

COPY requirements.txt ./requirements.temp.txt
RUN pip install -r requirements.temp.txt && \
    rm requirements.temp.txt