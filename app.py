import csv
from src.service import ImportService

def main():
    rows = []
    # TODO replace hardcode with argparse
    with open('docs/dealer_import.csv', 'rt') as csvfile:
        reader = csv.DictReader(csvfile)
        rows = list(reader)
    service = ImportService()
    results = service.import_rows(rows)
    print(results)

if __name__ == '__main__':
    main()